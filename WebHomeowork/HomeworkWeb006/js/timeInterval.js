setInterval(() => {
    let date = new Date();
    document.getElementById('day').innerHTML = date.toDateString();
    document.getElementById('time').innerHTML = date.toLocaleTimeString();
}, 1000)