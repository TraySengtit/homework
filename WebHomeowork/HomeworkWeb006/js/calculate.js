var hStart;
var mStart;
var hStop;
var mStop;

function Start() {
    const cDate = new Date();
    hStart = cDate.getHours();
    mStart = cDate.getMinutes();
    document.getElementById('btnstart').style.display = 'none'
    document.getElementById('btnstop').style.display = 'block'

    document.getElementById('startTime').innerHTML = hStart + " : " + mStart;
}

function Stop() {
    const aDate = new Date();
    hStop = aDate.getHours();
    mStop = aDate.getMinutes();
    document.getElementById('btnstop').style.display = 'none'
    document.getElementById('btnclear').style.display = 'block'
    document.getElementById('stopTime').innerHTML = hStop + " : " + mStop;
    var totalHour = (hStop - hStart) * 60;
    var totalMin = mStop - mStart;
    var total = totalHour + totalMin;
    document.getElementById('totalTime').innerHTML = `${total} `
    if (total <= 15) {
        document.getElementById('money').innerHTML = '500 '
    } else if (total > 15 && total <= 30) {
        document.getElementById('money').innerHTML = '1000 '
    } else if (total > 31 && total <= 60) {
        document.getElementById('money').innerHTML = '1500 '
    } else if (total > 60 && total <= 75) {
        document.getElementById('money').innerHTML = '2000 '
    } else if (total > 75 && total <= 90) {
        document.getElementById('money').innerHTML = '2500 '
    } else if (total > 90 && total <= 120) {
        document.getElementById('money').innerHTML = '3000 '
    } else if (total > 120 && total <= 135) {
        document.getElementById('money').innerHTML = '3500 '
    } else if (total > 135 && total <= 150) {
        document.getElementById('money').innerHTML = '4000 '
    } else if (total > 150 && total <= 180) {
        document.getElementById('money').innerHTML = '4500 '
    } else if (total > 180 && total <= 195) {
        document.getElementById('money').innerHTML = '5000 '
    } else if (total > 195 && total <= 210) {
        document.getElementById('money').innerHTML = '5500 '
    } else if (total > 210 && total <= 240) {
        document.getElementById('money').innerHTML = '6000 '
    }
}

function Clear() {
    document.getElementById('btnclear').style.display = 'none'
    document.getElementById('btnstart').style.display = 'block'
    document.getElementById('startTime').innerHTML = '0:00'
    document.getElementById('stopTime').innerHTML = '0:00'
    document.getElementById('totalTime').innerHTML = '0 '
    document.getElementById('money').innerHTML = '0 '
}